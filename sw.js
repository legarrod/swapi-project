let url = 'https://swapi.co/api/films/'; //la URL a usar la guardamos en una variable


let xmlhttp = new XMLHttpRequest();//instansiamos el XML objeto de javascript que nos permite
//obtener informacion de una URL

 xmlhttp.open('GET', url, true);//indicamos el metodo, pasamos la URL y le indicamos si es asincrono
 xmlhttp.send();//send metodo para enviar

xmlhttp.onreadystatechange = function() {//disparamos el evento con ready, el cual viene del xmlhttp
    //y nos ayuda a retornar una funcion
    if (this.readyState == 4 && this.status == 200) {//debemos validar el statur y el readyState
        
        var myObject = JSON.parse(this.responseText);//con esto hacemos que la informacion de la api 
        //sea legible para javascript
        //console.log(myObject.results); //con esta linea probamos que la api esta conectada correctamente
      let r = myObject.results;
        for (let j = 0; j < r.length; j++) {//creamos un ciclo para iterar los el array obtenido de la api
         //console.log(r[j]); // estos dos console log son de pruebas
          //document.write(r[j].title);
     
            for(let i of myObject.results){//creamos un ciclo, para recorrer la api, asi poder seleccionar indice
                //que necesitamos
                
                //creamos una variable card para almacenar el formato del card div de bootstrap 
                var card = `<div class="card mb-3 col-lg-4 col-md-6 col-xl-4">
                    <h3 class="card-header" id="titulo">${r[j].title }</h3>
                <div class="card-body">
                <h5 class="card-title" id="director">${ 'Director: '+r[j].director  }</h5>
                
                <h6 class="card-subtitle text-muted" id="episode_id">${'Episodio: '+r[j].episode_id }</h6>
                </div>
                <div class="card-body">
                <p class="card-text" id="opening_crawl">${ r[j].opening_crawl }</p>
                </div>
                <div class="card-footer text-muted" id="release_date">
                ${ 'Fecha de filmacion: '+r[j].release_date }
                </div>
                </div>`; //en ${}pasamos los valor del JSON que queremos mostrar
                
                    
                    
            }$(".row").append(card);// aca le pasamos al div row el resultado de los valores iterados
        }
    }
};







